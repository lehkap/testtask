﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ExpressionInterpritator.Client;
using ExpressionInterpritator.Expressions;
using ExpressionStringWorker;
using System.Collections.Generic;
using ExpressionStringWorker.Exceptions;

namespace ExpressionCalculatorTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void SimpleTest()
        {
            ExpressionConstractor constr = new ExpressionConstractor();
            var exp = constr.Constract("10+10/2-5-5*3");
            double x = exp.Calculate(new Context());
            double y = -5;
            Assert.AreEqual(y, x);
        }
        [TestMethod]
        public void BracketTest()
        {
            ExpressionConstractor constr = new ExpressionConstractor();
            var exp = constr.Constract("2+2*2");
            double x = exp.Calculate(new Context());
            double y = 6;
            Assert.AreEqual(y, x);
            exp = constr.Constract("(2+2)*2");
            x = exp.Calculate(new Context());
            y = 8;
            Assert.AreEqual(y, x);
        }
        [TestMethod]
        public void VariableTest()
        {
            ExpressionConstractor constr = new ExpressionConstractor();
            var exp = constr.Constract("2+2*b");
            Context context = new Context();
            context.AddVariable("b", 3);
            double x = exp.Calculate(context);
            double y = 8;
            Assert.AreEqual(y, x);
        }
        [TestMethod]
        public void UnarMinusTest()
        {
            ExpressionConstractor constr = new ExpressionConstractor();
            var exp = constr.Constract("-5+(-6)");
            double x = exp.Calculate(new Context());
            double y = -11;
            Assert.AreEqual(y, x);
        }

        [TestMethod]
        public void validStringTest()
        {
            StringProperties prop = new StringProperties("a45=-9*R-n10+(67+2)/R");
            string name = "a45";
            string body = "-9*R-n10+(67+2)/R";
            List<string> varList = new List<string>();
            varList.Add("R");
            varList.Add("n10");
            Assert.AreEqual(name, prop.Name);
            Assert.AreEqual(body, prop.Body);
            Assert.AreEqual(varList.Count, prop.Variables.Count);
            for (int i = 0; i < varList.Count; i++)
            {
                Assert.AreEqual(varList[i], prop.Variables[i]);
            }
        }
        [TestMethod]
        [ExpectedException(typeof(EqualSignException))]
        public void ExceptionStringTest()
        {
            StringProperties prop = new StringProperties("a45=-9*=R-n10+(67+2)/R");
            
        }

        [TestMethod]
        [ExpectedException(typeof(EqualSignException))]
        public void ExceptionStringTest2()
        {
            StringProperties prop = new StringProperties("a45-9*R-n10+(67+2)/R");

        }
        [TestMethod]
        [ExpectedException(typeof(WrongSignOrderException))]
        public void ExceptionStringTest3()
        {
            StringProperties prop = new StringProperties("a45=-9*R-n10+*(67+2)/R");

        }
        [TestMethod]
        [ExpectedException(typeof(ExpressionStringWorkerException))]
        public void ExceptionStringTest4()
        {
            StringProperties prop = new StringProperties("a4-5=-9*R-n10*(67+2)/R");

        }
    }
}
