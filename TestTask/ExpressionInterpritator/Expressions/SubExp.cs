﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Expressions
{
    public class SubExp : Operand
    {
        public SubExp()
        {
            Priority = 2;
            Demention = 2;
        }
        public override double Calculate(Context context)
        {
            return _members.Pop().Calculate(context) - _members.Pop().Calculate(context);
        }
    }
}
