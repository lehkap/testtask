﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Expressions
{
    class UnarMinus : Operand
    {        
        public UnarMinus()
        {
            Priority =5 ;
            Demention = 1;
        }

        public override double Calculate(Context context)
        {
            return _members.Pop().Calculate(context)*(-1);
        }       
    }
}
