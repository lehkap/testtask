﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Expressions
{
    public class Context
    {
        public double this[string variableName]
        {
            get
            {
                return _variableValues[variableName];
            }
        }
        public int Count
        {
            get
            {
                return _variableValues.Count;
            }
        }
        public void AddVariable(string name, double value)
        {
            _variableValues.Add(name, value);
        }
        private Dictionary<string, double> _variableValues = new Dictionary<string, double>();

    }
}
