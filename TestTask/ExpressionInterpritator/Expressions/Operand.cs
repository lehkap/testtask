﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Expressions
{
    /// <summary>
    /// Выражение
    /// </summary>
    public abstract class Operand:IExpression
    { 
        public void AddExpressionMember(IExpression expression)
        {
            _members.Push(expression);
        }
        /// <summary>
        /// Приоритет выполнения
        /// </summary>
        public virtual int Priority { get; protected set; }

        public virtual int Demention { get; protected set; }

        public abstract double Calculate(Context context);

        protected Stack<IExpression> _members=new Stack<IExpression>();
    }
}
