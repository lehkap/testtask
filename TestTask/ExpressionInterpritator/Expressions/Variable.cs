﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Expressions
{
    class Variable : IExpression
    {
        public Variable(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
        public double Calculate(Context context)
        {
            return context[Name];
        }
    }
}
