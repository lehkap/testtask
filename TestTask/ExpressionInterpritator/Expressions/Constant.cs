﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Expressions
{
    public class Constant : IExpression
    {
        public Constant(string strConst)
        {
            SetValue(strConst);
        }
        private double _value;
        private void SetValue(string strValue)
        {
            _value = double.Parse(strValue, CultureInfo.InvariantCulture);
        }
        public double Calculate(Context context)
        {
            return _value;
        }
    }
}
