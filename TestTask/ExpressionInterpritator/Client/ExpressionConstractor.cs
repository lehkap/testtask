﻿using ExpressionInterpritator.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Client
{
    public class ExpressionConstractor
    {


        Stack<IExpression> _output;
        Stack<Operand> _expressionStack;        
        /// <summary>
        /// Преобразует строку в выражение
        /// </summary>
        /// <param name="strExpression">Выражение в виде строки</param>
        /// <returns>Выражение</returns>
        public IExpression Constract(string strExpression)
        {
            string[] signs = { "+", "-", "*", "/", "(", ")" };
            Regex regex = new Regex(@"(?<UnarMinus>(?<![\w)])[-])|(?<Other>(?<![\w]+)[-][\d.]+|[\w.]+|[)(*/+-])");
            _output = new Stack<IExpression>();
            _expressionStack = new Stack<Operand>();
            var _splitExpression = regex.Matches(strExpression);
           
            for (int i = 0; i < _splitExpression.Count; i++)
            {
                //string currentOperator = splitExpression[i].ToString();
                switch (_splitExpression[i].Value)
                {
                    case "+":                    
                    case "*":
                    case "/":
                    case "(":
                        Operand operand = GetExp(_splitExpression[i].Value);
                        ProcessOperand(operand);
                        break;
                    case "-":
                        if (_splitExpression[i].Groups["UnarMinus"].Success)
                        {
                            ProcessOperand(new UnarMinus());
                        }else
                        {
                            ProcessOperand(new SubExp());
                        }
                        break;
                    case ")":
                        while (!(_expressionStack.Peek() is LeftBracket))
                        {
                            for (int j = 0; j < _expressionStack.Peek().Demention; j++)
                            {
                                (_expressionStack.Peek() as Operand).AddExpressionMember(_output.Pop());
                            }
                            _output.Push(_expressionStack.Pop());
                        }
                        _expressionStack.Pop();                       
                        break;
                    default:
                        Regex varRegex = new Regex(@"[a-zA-Z]");
                        if (varRegex.IsMatch(_splitExpression[i].Value))
                        {
                            _output.Push(new Variable(_splitExpression[i].Value));
                        }
                        else
                        {
                            _output.Push(new Constant(_splitExpression[i].Value));
                        }
                        break;
                }
            }
            while (_expressionStack.Count > 0)
            {
                for (int i = 0; i < _expressionStack.Peek().Demention; i++)
                {
                    (_expressionStack.Peek() as Operand).AddExpressionMember(_output.Pop());
                }
                _output.Push(_expressionStack.Pop());
            }
            return _output.Pop();
        }
        /// <summary>
        /// Создает экземпляр выражения по имени
        /// </summary>
        /// <param name="strOperator"></param>
        /// <returns></returns>
        private Operand GetExp(string strOperator)
        {
            switch (strOperator)
            {
                case "+":
                    return new AddExp();
                case "-":
                    return new SubExp();              
                case "*":
                    return new MulExp();
                case "/":
                    return new DivExp();
                case "(":
                    return new LeftBracket();                                           
            }
            throw new Exception();
        }

        private void ProcessOperand(Operand operand)
        {
            
            if (_expressionStack.Count > 0)
            {
                if (operand.Priority > _expressionStack.Peek().Priority)
                {
                    _expressionStack.Push(operand);
                }
                else
                {
                    do
                    {
                        for (int i = 0; i < _expressionStack.Peek().Demention; i++)
                        {
                            (_expressionStack.Peek() as Operand).AddExpressionMember(_output.Pop());
                        }
                        _output.Push(_expressionStack.Pop());
                    } while (_expressionStack.Count > 0 && _expressionStack.Peek().Priority > operand.Priority);
                    _expressionStack.Push(operand);
                }
            }
            else
            {
                _expressionStack.Push(operand);
                if (_expressionStack.Peek() is LeftBracket)
                {
                    (_expressionStack.Peek() as LeftBracket).ResetPriority();
                }
            }
        }        
    }
}
