﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Exceptions
{

    [Serializable]
    public class WrongOperatorPositionException : ExpressionInterpritatorException
    {
        public WrongOperatorPositionException() { }
        public WrongOperatorPositionException(string message) : base(message) { }
        public WrongOperatorPositionException(string message, Exception inner) : base(message, inner) { }
        protected WrongOperatorPositionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
