﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInterpritator.Exceptions
{
    [System.Serializable]
    public class ExpressionInterpritatorException : Exception
    {
        public ExpressionInterpritatorException() { }
        public ExpressionInterpritatorException(string message) : base(message) { }
        public ExpressionInterpritatorException(string message, Exception inner) : base(message, inner) { }
        protected ExpressionInterpritatorException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
    

