﻿using ExpressionServer.Requests;
using ExpressionServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExpressionServer
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IService1" в коде и файле конфигурации.
    [ServiceContract]
    public interface IServiceEditor
    {
        /// <summary>
        /// Добавить выражение
        /// </summary>
        /// <param name="request">Запрос</param>
        /// <returns>Ответ от сервера</returns>
        [OperationContract]
        AddExpressionResponse AddExpression(AddExpressionRequest request);
    }
} 
