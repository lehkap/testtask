﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ExpressionServer.Requests;
using ExpressionServer.Responses;
using ExpressionServer.Entities;

namespace ExpressionServer
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде и файле конфигурации.
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, InstanceContextMode = InstanceContextMode.Single)]
    public class Service : IServiceEditor
    {
        MyContext _myContext = new MyContext();                

        public AddExpressionResponse AddExpression(AddExpressionRequest request)
        {
            Expression expression = new Expression(request.Expresssion);
            _myContext.Expressions.Add(expression);
            _myContext.SaveChanges();
            return new AddExpressionResponse()
            {
                Status = Responses.Elements.Status.Ok
            };
        }
    }
}
