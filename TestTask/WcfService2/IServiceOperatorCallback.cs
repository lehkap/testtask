﻿using ExpressionServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer
{
    interface IServiceOperatorCallback
    {
        void RecieveExpressionValue(GetExpressionValueResponse response);
    }
}
