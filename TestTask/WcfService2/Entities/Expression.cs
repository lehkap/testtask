﻿using ExpressionStringWorker;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer.Entities
{
    public class Expression
    {
        public int Id { get; set; }        
        public string Name { get; set; }
        public string Body { get; set; }
        public double Value { get; set; }
        public ICollection<Variable> Variables { get; set; }        
        public Expression(string expressionString)
        {
            List<Variable> varibles = new List<Variable>();
            StringProperties expressionProperties = new StringProperties(expressionString);
            foreach (var item in expressionProperties.Variables)
            {
                varibles.Add(new Variable(item));
            }
            Name = expressionProperties.Name;
            Body = expressionProperties.Body;
            Variables = varibles;
        }
    }
}
