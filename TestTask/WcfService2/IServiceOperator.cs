﻿using ExpressionServer.Requests;
using ExpressionServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer
{
    [ServiceContract(CallbackContract =typeof(IServiceOperatorCallback))]
    interface IServiceOperator
    {
        [OperationContract]
        GetExpressionValueResponse GetExpressionValue(GetExpressionValueRequest requset);
    }
}
