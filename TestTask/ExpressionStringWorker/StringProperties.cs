﻿using ExpressionStringWorker.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExpressionStringWorker
{
    public class StringProperties
    {
        /// <summary>
        /// Имя функции
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Тело функции
        /// </summary>
        public string Body { get; private set; }
        /// <summary>
        /// Коллекция переменных в строке
        /// </summary>
        public List<string> Variables;
        public StringProperties(string strExpression)
        {
            string[] splitStr = strExpression.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
            if (splitStr.Length!=2)
            {
                throw new EqualSignException("Неверное количество '=' в выражении");
            }
            Regex regex = new Regex(@"[^\w]");
            if (regex.IsMatch(splitStr[0]))
            {
                throw new ExpressionStringWorkerException("Неверное имя функции");
            }
            Name = splitStr[0];
            Body = splitStr[1];
            Verify(Body);
            Variables = FindVariables(Body);

        }
        /// <summary>
        /// Ищет переменные в выражении
        /// </summary>
        /// <param name="strExpBody">Строковое выражение</param>
        /// <returns>Коллекция имен переменных</returns>
        private List<string> FindVariables(string strExpBody)
        {
            List<string> result = new List<string>();
            Regex varAndConst = new Regex(@"(?<![\w]+)[\d.]+|[\w]+");
            Regex chars = new Regex(@"[a-zA-Z]+");
            MatchCollection matches = varAndConst.Matches(strExpBody);
            foreach (var item in matches)
            {
                if (chars.IsMatch(item.ToString()))
                {
                    if (!result.Contains(item.ToString()))
                    {
                        result.Add(item.ToString());
                    }
                }                                
            }
            return result;
        }
        /// <summary>
        /// Проверяем строку на валидность
        /// </summary>
        /// <param name="strExpression"></param>
        private void Verify(string strExpression)
        {
            Regex wrongOperandRegex = new Regex(@"[\(/*+-][)/*+]");
            if (wrongOperandRegex.IsMatch(strExpression))
            {
                throw new WrongSignOrderException("Неверный порядок операторов в выражении");
            }
            Regex leftBracket = new Regex(@"[(]");
            Regex rightBracket = new Regex(@"[)]");
            if (leftBracket.Matches(strExpression).Count!=rightBracket.Matches(strExpression).Count)
            {
                throw new WrongSignOrderException("Не все скобки имя открывающую или закрывающую пару");
            }
        }

        
    }
}
