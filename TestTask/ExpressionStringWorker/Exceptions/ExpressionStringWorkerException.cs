﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionStringWorker.Exceptions
{

    [Serializable]
    public class ExpressionStringWorkerException : Exception
    {
        public ExpressionStringWorkerException() { }
        public ExpressionStringWorkerException(string message) : base(message) { }
        public ExpressionStringWorkerException(string message, Exception inner) : base(message, inner) { }
        protected ExpressionStringWorkerException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
