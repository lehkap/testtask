﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionStringWorker.Exceptions
{

    [Serializable]
    public class EqualSignException : ExpressionStringWorkerException
    {
        public EqualSignException() { }
        public EqualSignException(string message) : base(message) { }
        public EqualSignException(string message, Exception inner) : base(message, inner) { }
        protected EqualSignException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
