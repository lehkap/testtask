﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionStringWorker.Exceptions
{

    [Serializable]
    public class WrongSignOrderException : ExpressionStringWorkerException
    {
        public WrongSignOrderException() { }
        public WrongSignOrderException(string message) : base(message) { }
        public WrongSignOrderException(string message, Exception inner) : base(message, inner) { }
        protected WrongSignOrderException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
