﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EditorClient
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var client = new ServiceEditor.ServiceEditorClient("NetTcpBinding_IServiceEditor"))
            {
                while (true)
                {
                    Console.Write("Введите выражение: ");
                    var e = Console.ReadLine();
                    ServiceEditor.AddExpressionRequest request = new ServiceEditor.AddExpressionRequest() { Expresssion = e };
                    var r = client.AddExpression(request);
                    if (r.Status != ServiceEditor.Status.Ok)
                    {
                        Console.WriteLine(r.Message);
                    }
                }
            }
        }
    }
}
