﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Variable
    {
        public int Id { get; set; }                
        public string VarName { get; set; }                           
        public int ExpressionId { get; set; }
        public Expression Expression { get; set; }  
        public Variable() { }      
        public Variable(string variable)
        {
            VarName = variable;
        }
    }
}
