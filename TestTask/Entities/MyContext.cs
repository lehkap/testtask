﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class MyContext:DbContext
    {
        public MyContext() : base() { }
        public DbSet<Expression> Expressions { get; set; }
        public DbSet<Variable> Variables { get; set; }
    }
}
