﻿using ExpressionStringWorker;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Expression
    {
        [Key] 
        [Column(Order =1)]   
        public int Id { get; set; }        
        public string Name { get; set; }
        public string Body { get; set; }        
        public double? Value { get; set; }        
        public ICollection<Variable> Variables { get; set; }
        public Expression() { }       
        public Expression(string expressionString)
        {
            StringProperties expressionProperties = new StringProperties(expressionString);
            List<Variable> varibles = new List<Variable>();            
            foreach (var item in expressionProperties.Variables)
            {
                varibles.Add(new Variable(item));
            }
            Name = expressionProperties.Name;
            Body = expressionProperties.Body;
            Variables = varibles;
        }
    }
}
