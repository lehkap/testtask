﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpressionServer;
using System.ServiceModel;
using Entities;

namespace Host
{
    class Program
    {
        static void Main(string[] args)
        {
            MyContext myContext = new MyContext();
            myContext.Database.Delete();
            myContext.Database.CreateIfNotExists();
            using (var host = new ServiceHost(typeof(Service)))
            {
                host.Open();
                Console.WriteLine("Сервис запущен");
                Console.ReadLine();
            }
        }
    }
}
