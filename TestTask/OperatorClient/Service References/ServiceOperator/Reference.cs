﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OperatorClient.ServiceOperator {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BaseRequest", Namespace="http://schemas.datacontract.org/2004/07/ExpressionServer.Requests")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(OperatorClient.ServiceOperator.GetExpressionValueRequest))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(OperatorClient.ServiceOperator.AddExpressionRequest))]
    public partial class BaseRequest : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetExpressionValueRequest", Namespace="http://schemas.datacontract.org/2004/07/ExpressionServer.Requests")]
    [System.SerializableAttribute()]
    public partial class GetExpressionValueRequest : OperatorClient.ServiceOperator.BaseRequest {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ExpressionNameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ExpressionName {
            get {
                return this.ExpressionNameField;
            }
            set {
                if ((object.ReferenceEquals(this.ExpressionNameField, value) != true)) {
                    this.ExpressionNameField = value;
                    this.RaisePropertyChanged("ExpressionName");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="AddExpressionRequest", Namespace="http://schemas.datacontract.org/2004/07/ExpressionServer.Requests")]
    [System.SerializableAttribute()]
    public partial class AddExpressionRequest : OperatorClient.ServiceOperator.BaseRequest {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ExpresssionField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Expresssion {
            get {
                return this.ExpresssionField;
            }
            set {
                if ((object.ReferenceEquals(this.ExpresssionField, value) != true)) {
                    this.ExpresssionField = value;
                    this.RaisePropertyChanged("Expresssion");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BaseResponse", Namespace="http://schemas.datacontract.org/2004/07/ExpressionServer.Responses")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(OperatorClient.ServiceOperator.GetExpressionValueResponse))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(OperatorClient.ServiceOperator.AddExpressionResponse))]
    public partial class BaseResponse : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private OperatorClient.ServiceOperator.Status StatusField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public OperatorClient.ServiceOperator.Status Status {
            get {
                return this.StatusField;
            }
            set {
                if ((this.StatusField.Equals(value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetExpressionValueResponse", Namespace="http://schemas.datacontract.org/2004/07/ExpressionServer.Responses")]
    [System.SerializableAttribute()]
    public partial class GetExpressionValueResponse : OperatorClient.ServiceOperator.BaseResponse {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double ValueField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Value {
            get {
                return this.ValueField;
            }
            set {
                if ((this.ValueField.Equals(value) != true)) {
                    this.ValueField = value;
                    this.RaisePropertyChanged("Value");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="AddExpressionResponse", Namespace="http://schemas.datacontract.org/2004/07/ExpressionServer.Responses")]
    [System.SerializableAttribute()]
    public partial class AddExpressionResponse : OperatorClient.ServiceOperator.BaseResponse {
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Status", Namespace="http://schemas.datacontract.org/2004/07/ExpressionServer.Responses.Elements")]
    public enum Status : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Ok = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Warning = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Error = 2,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceOperator.IServiceEditor")]
    public interface IServiceEditor {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceEditor/AddExpression", ReplyAction="http://tempuri.org/IServiceEditor/AddExpressionResponse")]
        OperatorClient.ServiceOperator.AddExpressionResponse AddExpression(OperatorClient.ServiceOperator.AddExpressionRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceEditor/AddExpression", ReplyAction="http://tempuri.org/IServiceEditor/AddExpressionResponse")]
        System.Threading.Tasks.Task<OperatorClient.ServiceOperator.AddExpressionResponse> AddExpressionAsync(OperatorClient.ServiceOperator.AddExpressionRequest request);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceEditorChannel : OperatorClient.ServiceOperator.IServiceEditor, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceEditorClient : System.ServiceModel.ClientBase<OperatorClient.ServiceOperator.IServiceEditor>, OperatorClient.ServiceOperator.IServiceEditor {
        
        public ServiceEditorClient() {
        }
        
        public ServiceEditorClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceEditorClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceEditorClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceEditorClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public OperatorClient.ServiceOperator.AddExpressionResponse AddExpression(OperatorClient.ServiceOperator.AddExpressionRequest request) {
            return base.Channel.AddExpression(request);
        }
        
        public System.Threading.Tasks.Task<OperatorClient.ServiceOperator.AddExpressionResponse> AddExpressionAsync(OperatorClient.ServiceOperator.AddExpressionRequest request) {
            return base.Channel.AddExpressionAsync(request);
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceOperator.IServiceOperator", CallbackContract=typeof(OperatorClient.ServiceOperator.IServiceOperatorCallback))]
    public interface IServiceOperator {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IServiceOperator/GetExpressionValue")]
        void GetExpressionValue(OperatorClient.ServiceOperator.GetExpressionValueRequest requset);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IServiceOperator/GetExpressionValue")]
        System.Threading.Tasks.Task GetExpressionValueAsync(OperatorClient.ServiceOperator.GetExpressionValueRequest requset);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceOperatorCallback {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IServiceOperator/RecieveExpressionValue")]
        void RecieveExpressionValue(OperatorClient.ServiceOperator.GetExpressionValueResponse response);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceOperatorChannel : OperatorClient.ServiceOperator.IServiceOperator, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceOperatorClient : System.ServiceModel.DuplexClientBase<OperatorClient.ServiceOperator.IServiceOperator>, OperatorClient.ServiceOperator.IServiceOperator {
        
        public ServiceOperatorClient(System.ServiceModel.InstanceContext callbackInstance) : 
                base(callbackInstance) {
        }
        
        public ServiceOperatorClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName) : 
                base(callbackInstance, endpointConfigurationName) {
        }
        
        public ServiceOperatorClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceOperatorClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceOperatorClient(System.ServiceModel.InstanceContext callbackInstance, System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, binding, remoteAddress) {
        }
        
        public void GetExpressionValue(OperatorClient.ServiceOperator.GetExpressionValueRequest requset) {
            base.Channel.GetExpressionValue(requset);
        }
        
        public System.Threading.Tasks.Task GetExpressionValueAsync(OperatorClient.ServiceOperator.GetExpressionValueRequest requset) {
            return base.Channel.GetExpressionValueAsync(requset);
        }
    }
}
