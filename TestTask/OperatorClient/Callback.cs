﻿using OperatorClient.ServiceOperator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace OperatorClient
{
    [CallbackBehavior(UseSynchronizationContext = false)]
    class Callback : IServiceOperatorCallback
    {
        public delegate void Recieve(GetExpressionValueResponse response);
        public event Recieve OnRecieve;       
        public void RecieveExpressionValue(GetExpressionValueResponse response)
        {
            OnRecieve(response);
        }
    }
    
}
