﻿using OperatorClient.ServiceOperator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OperatorClient
{
    class Program
    {
        private static ManualResetEvent waitHandle = new ManualResetEvent(false);
        static void Main(string[] args)
        {
            Callback callback = new Callback();
            InstanceContext context = new InstanceContext(callback);
            using (var client = new ServiceOperator.ServiceOperatorClient(context, "NetTcpBinding_IServiceOperator"))
            {
                callback.OnRecieve += Callback_OnRecieve;
                while (true)
                {
                    Thread.Sleep(100);
                    Console.WriteLine("Введите имя выражения ");
                    string e =Console.ReadLine();
                    var request = new GetExpressionValueRequest()
                    {
                        ExpressionName = e
                    };
                    client.GetExpressionValue(request);
                    waitHandle.WaitOne();
                }                
            }
        }

        private static void Callback_OnRecieve(GetExpressionValueResponse response)
        {
            if (response.Status==Status.Ok)
            {
                Console.WriteLine("Результат: "+response.Value);
            }
            else
            {
                Console.WriteLine("Ошибка: " + response.Message);
            }
            waitHandle.Set();
        }
    }
}
