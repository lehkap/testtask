﻿using Entities;
using ExpressionInterpritator.Client;
using ExpressionInterpritator.Expressions;
using ExpressionServer.Requests;
using ExpressionServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer
{
    partial class Service
    {
        private AddExpressionResponse AddExpressionBase(AddExpressionRequest request)
        {
            Expression expression = new Expression(request.Expresssion);

            if (_myContext.Expressions.Where(e => e.Name == expression.Name).Count()>0)
            {               
                return new AddExpressionResponse()
                {
                    Status = Responses.Elements.Status.Warning,
                    Message = "Выражение с таким именем уже существут"
                };
            }
            expression.Value = CalcuateExpression(expression);
            _myContext.Expressions.Add(expression);
            _myContext.SaveChanges();
            if (expression.Value!=null)
            {
                RefreshCalculation(expression.Name);
                //GetAwaitingExpression(expression);
            }
            return new AddExpressionResponse()
            {
                Status = Responses.Elements.Status.Ok
            };
        }

        private double? CalcuateExpression(Expression expression)
        {
            ExpressionConstractor constractor = new ExpressionConstractor();
            IExpression exp = constractor.Constract(expression.Body);
            if (expression.Variables.Count == 0)
            {                
                return exp.Calculate(new Context());
            }
            else
            {
                Context context = new Context();
                foreach (var item in expression.Variables)
                {
                    var variable = _myContext.Expressions.FirstOrDefault(e => e.Name == item.VarName);
                    if (variable!=null)
                    {
                        context.AddVariable(variable.Name, (double)variable.Value);
                    }
                }
                if (context.Count==expression.Variables.Count)
                {
                    return exp.Calculate(context);
                }
                else
                {
                    return null;
                }
            }
        }

        private void RefreshCalculation(string guiltiExpName)
        {
            var variables =_myContext.Variables.Where(v => v.VarName == guiltiExpName).ToList();
            foreach (var item in variables)
            {
                Expression expression = _myContext.Expressions.First(e => e.Id==item.ExpressionId);
                expression.Value = CalcuateExpression(expression);
                GetAwaitingExpression(expression);
            }
            _myContext.SaveChanges();
            
        }

        private void GetAwaitingExpression(Expression expression)
        {
            if (_awaitingClients.ContainsKey(expression.Name))
            {
                foreach (var item in _awaitingClients[expression.Name])
                {
                    GetExpressionValueResponse response = new GetExpressionValueResponse()
                    {
                        Value = (double)expression.Value
                    };
                    item.RecieveExpressionValue(response);
                }
                _awaitingClients.Remove(expression.Name);
            }
        }
    }
}
