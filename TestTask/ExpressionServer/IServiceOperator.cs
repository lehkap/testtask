﻿using ExpressionServer.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer
{
    [ServiceContract(CallbackContract = typeof(IServiceOperatorCallback))]
    interface IServiceOperator
    {
        [OperationContract(IsOneWay =true)]
        void GetExpressionValue(GetExpressionValueRequest requset);
    }
}
