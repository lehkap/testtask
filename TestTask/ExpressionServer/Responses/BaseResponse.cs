﻿using ExpressionServer.Responses.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer.Responses
{
    [DataContract]
    public class BaseResponse
    {
        /// <summary>
        /// Статус выполнения операции
        /// </summary>
        [DataMember]
        public Status Status { get; set; }

        /// <summary>
        /// Сопровоздающие сообщение
        /// </summary>
        [DataMember]
        public string Message { get; set; }
    }
}
