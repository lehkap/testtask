﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer.Responses
{
    [DataContract]
    public class GetExpressionValueResponse:BaseResponse
    {
        [DataMember]
        public double Value { get; set; }
    }
}
