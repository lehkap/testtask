﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer.Responses.Elements
{
    [DataContract]
    public enum Status
    {
        [EnumMember]
        Ok,
        [EnumMember]
        Warning,
        [EnumMember]
        Error
    }
}
