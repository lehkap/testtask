﻿using ExpressionServer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer
{
    interface IServiceOperatorCallback
    {
        [OperationContract(IsOneWay =true)]
        void RecieveExpressionValue(GetExpressionValueResponse response);
    }
}
