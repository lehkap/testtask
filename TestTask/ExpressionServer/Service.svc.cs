﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ExpressionServer.Requests;
using ExpressionServer.Responses;
using Entities;
using System.Data.Entity.Infrastructure;
using ExpressionStringWorker.Exceptions;

namespace ExpressionServer
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде и файле конфигурации.
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, InstanceContextMode = InstanceContextMode.Single)]
    public partial class Service : IServiceEditor, IServiceOperator
    {
        MyContext _myContext = new MyContext();
        Dictionary<string, List<IServiceOperatorCallback>> _awaitingClients = new Dictionary<string, List<IServiceOperatorCallback>>();         
        public AddExpressionResponse AddExpression(AddExpressionRequest request)
        {
            try
            {
                return AddExpressionBase(request);                
            }
            catch (DbUpdateException)
            {
                return new AddExpressionResponse()
                {
                    Status = Responses.Elements.Status.Warning,
                    Message = "Выражение с таким именем уже существует"
                };
            }
            catch (ExpressionStringWorkerException ex)
            {
                return new AddExpressionResponse()
                {
                    Status = Responses.Elements.Status.Error,
                    Message = ex.Message
                };
            }
            
        }

        public void GetExpressionValue(GetExpressionValueRequest requset)
        {
            var y = OperationContext.Current.GetCallbackChannel<IServiceOperatorCallback>();
            var expression = _myContext.Expressions.First(e => e.Name == requset.ExpressionName);
            if (expression.Value!=null)
            {
                GetExpressionValueResponse response = new GetExpressionValueResponse()
                {
                    Status = Responses.Elements.Status.Ok,
                    Value = (double)expression.Value
                };
                OperationContext.Current.GetCallbackChannel<IServiceOperatorCallback>().RecieveExpressionValue(response);
            }
            else
            {
               if( _awaitingClients.ContainsKey(expression.Name))
                {                    
                    _awaitingClients[requset.ExpressionName].Add(OperationContext.Current.GetCallbackChannel<IServiceOperatorCallback>());
                }
               else
                {
                    _awaitingClients.Add(requset.ExpressionName, new List<IServiceOperatorCallback>());
                    _awaitingClients[requset.ExpressionName].Add(OperationContext.Current.GetCallbackChannel<IServiceOperatorCallback>());
                }
            }
        }
    }
}
