﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionServer.Requests
{
    [DataContract]
    public class GetExpressionValueRequest:BaseRequest
    {
        [DataMember]
        public string ExpressionName { get; set; }
    }
}
