﻿using ExpressionCalculator.MathOperators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExpressionCalculator
{
    public class Calculator
    {
        /// <summary>
        /// Вычисляет значение выражения методом Shunting-yard
        /// </summary>
        /// <param name="Expression">Выражение которое необходимо вычислить</param>
        /// <returns>Результат вычисления</returns>
        public double Calculate(string Expression)
        {
            string[] signs = { "+", "-", "*", "/", "(", ")" };
            Regex regex = new Regex(@"(?<!\d)[-][\d.]+|[\d.]+|[)(*/+-]");
            var splitExpression = regex.Matches(Expression);            
            Stack<double> output = new Stack<double>();
            Operators operators = new Operators();
            Stack<Operator> operatorsStack = new Stack<Operator>();
            for (int i = 0; i < splitExpression.Count; i++)
            {
                string currentMember = splitExpression[i].ToString();
                if (signs.Contains(splitExpression[i].ToString()))//Проверяем оператор ли это
                {
                    Operator currentOperator = operators[currentMember];
                    if (operatorsStack.Count>0)//проверяем наличие операторов в стеке
                    {
                        if (currentOperator.Ptioryty>operatorsStack.Peek().Ptioryty)
                        {
                            operatorsStack.Push(currentOperator);
                        }
                        else
                        {
                            do
                            {
                                output.Push(operatorsStack.Pop().Execute(y: output.Pop(), x: output.Pop()));
                            } while (operatorsStack.Count>0 && operatorsStack.Peek().Ptioryty > currentOperator.Ptioryty);
                            operatorsStack.Push(currentOperator);
                        }
                    }
                    else
                    {
                        operatorsStack.Push(currentOperator);
                    }
                }
                else
                {
                    output.Push(double.Parse(currentMember));
                }              
            }
            while(operatorsStack.Count>0)
            {
                output.Push(operatorsStack.Pop().Execute(y: output.Pop(), x: output.Pop()));
            }
            return output.Pop();
        }        
    }
}
