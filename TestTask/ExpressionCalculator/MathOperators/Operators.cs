﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionCalculator.MathOperators
{
    public class Operators
    {
        /// <summary>
        /// Возвращает конкретный экземпляр Operator по его имени
        /// </summary>
        /// <param name="name">Имя оператора</param>
        /// <returns>Оператор</returns>
        public Operator this[string name]
        {
            get
            {
                return _operators[name];
            }
        }
        /// <summary>
        /// Инициализирует список операторов
        /// </summary>
        public Operators()
        {
            _operators.Add("+", new Plus());
            _operators.Add("-", new Minus());
            _operators.Add("*", new Mutiple());
            _operators.Add("/", new Division());
        }
        private Dictionary<string, Operator> _operators = new Dictionary<string, Operator>();
    }
}
