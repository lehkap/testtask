﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionCalculator
{
    public abstract class Operator
    {
        public abstract int Ptioryty { get;}
        public abstract double Execute(double x, double y);
    }
}
