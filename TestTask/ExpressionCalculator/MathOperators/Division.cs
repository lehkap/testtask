﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionCalculator.MathOperators
{
    public class Division : Operator
    {
        public override int Ptioryty
        {
            get
            {
                return 3;
            }
        }

        public override double Execute(double x, double y)
        {
            return x/ y;
        }
    }
}
